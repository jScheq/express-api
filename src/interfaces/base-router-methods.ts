import { Request, Response } from "express";


export interface BaseRouterMethods {
    find(requset: Request, response: Response): Promise<void>;
    findOne(requset: Request, response: Response): Promise<void>;
    create(requset: Request, response: Response): Promise<void>;
    update(requset: Request, response: Response): Promise<void>;
    delete(requset: Request, response: Response): Promise<void>;
}

