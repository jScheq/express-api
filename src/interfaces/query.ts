export interface ParsedQuery {
    limit: number;
    offset: number;
    filter: {[key: string]: any};
    sort: {[key: string]: number};
}

