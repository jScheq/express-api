import { User } from "./user";
import { Document } from "mongoose";

export interface Article extends Document {
    name: string;
    slug: string;
    text: string;
    author: User;
    createdAt: string;
    updatedAt: string;
    category: { value: string }[];
    comments: { comment: string, date: string, author: User }[];
}


