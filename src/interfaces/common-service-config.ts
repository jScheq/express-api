import { QueryPopulateOptions } from "mongoose";


export interface CommonServiceConfig {
    /**
     * Needs for build Content-Range header
     */
    entityName?: string;

    /**
     * Checking duplicate entity with this field
     */
    checkExists?: string;

    /**
     * Populate embedded entity
     */
    populate?: QueryPopulateOptions | QueryPopulateOptions[];
}

