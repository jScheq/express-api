import article from "../models/article";
import RouterHandler from "./router-handler";
import { Article } from "../interfaces/article";


export default class ArticleRouterHandler extends RouterHandler<Article> {
    constructor() {
        super(
            article, 
            {
                entityName: 'articles',
                populate: {
                    path: 'author',
                    select: '-password'
                }
            }
        );
    }
}



