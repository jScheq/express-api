import { Router } from "express";
import { Model, Document } from "mongoose";
import CommonService from "../services/common-service";
import { BaseRouterMethods } from "../interfaces/base-router-methods";
import { CommonServiceConfig } from "../interfaces/common-service-config";


export default class RouterHandler<Q extends Document> extends CommonService<Q> implements BaseRouterMethods {

    protected _router: Router;

    public get router(): Router {
        return this._router;
    }

    constructor(
        model: Model<Q>,
        config: CommonServiceConfig
    ) {
        super(model, config);

        this._router = Router();

        this._router.get('/', this.find.bind(this));
        this._router.post('/', this.create.bind(this));
        this._router.get('/:id', this.findOne.bind(this));
        this._router.put('/:id', this.update.bind(this));
        this._router.delete('/:id', this.delete.bind(this));
    }
}

