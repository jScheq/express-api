import user from "../models/user";
import { User } from "../interfaces/user";
import RouterHandler from "./router-handler";


export default class UserRouterHandler extends RouterHandler<User> {
    constructor() {
        super(
            user, 
            {
                entityName: 'users',
                checkExists: 'email'
            }
        );
    }
}

