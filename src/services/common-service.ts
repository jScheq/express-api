import { Request, Response } from "express";
import { ParsedQuery } from '../interfaces/query';
import { Model, Document, QueryPopulateOptions } from 'mongoose';
import { BaseRouterMethods } from "../interfaces/base-router-methods";
import { CommonServiceConfig } from '../interfaces/common-service-config';


export default abstract class CommonService<T extends Document> implements BaseRouterMethods {

    private _model: Model<T>;
    private _entityName: string;
    private _checkExists: string;
    private _populate: QueryPopulateOptions | QueryPopulateOptions[];

    constructor(
        model: Model<T>,
        config: CommonServiceConfig
    ) {
        this._model = model;

        this._checkExists = config.checkExists;
        this._entityName = config.entityName || 'entity';
        this._populate = config.populate || { path: '' };
    }

    async find(req: Request, res: Response): Promise<void> {
        try {
            const { limit, offset, filter, sort }: ParsedQuery = this._parseRequsetQuery(req.query);
    
            const entities: T[] = await this._model
                .find(filter)
                .skip(offset)
                .limit(limit)
                .sort(sort)
                .populate(this._populate);
    
            if (limit > 0) {
              //Content-Range: <unit> <range-start>-<range-end>/<size>
              const total: number = await this._model.count(filter);
              const contenRangeHeader: string = `${this._entityName} ${entities.length ? offset + 1 : offset}-${offset + limit < total ? offset + limit : total}/${total}`;
              res.setHeader('Content-Range', contenRangeHeader);
            }
    
            res.json(entities);
          } catch (error) {
            res.status(500).json({ error });
          }
    }

    async create(req: Request, res: Response): Promise<void> {
        try {
            if (this._checkExists) {
                const exists: boolean = !!(await this._model.find({ [this._checkExists]: req.body[this._checkExists] })).length
        
                if (exists) {
                  res.status(400).json({
                    error: true,
                    message: `${this._entityName} with ${req.body[this._checkExists]} ${this._checkExists} already exists.`
                  });
          
                  return;
                }
            }

            const savedEntity: T = await new this._model(req.body).save();
            res.json(savedEntity);
        } catch (error) {
            res.status(500).json({ error });
        }
    }

    async findOne(req: Request, res: Response): Promise<void> {
        try {
            const entity: T = await this._model
                .findById(req.params.id)
                .populate(this._populate);
    
            if (!entity) {
                res.sendStatus(404);
                return;
            }
    
            res.json(entity);
        } catch (error) {
            res.status(500).json({ error });
        }
    }

    async update(req: Request, res: Response): Promise<void> {
        try {
            const updatedEntity: T = await this._model.findOneAndUpdate(
                { _id: req.params.id },
                req.body,
                { new: true }
            );
            res.json(updatedEntity);
        } catch (error) {
            res.status(500).json({ error });
        }
    }

    async delete(req: Request, res: Response): Promise<void> {
        try {
            const removedEntity: {ok?: number, n?: number} = await this._model.deleteOne({ _id: req.params.id }); 
    
            if (removedEntity.n === 0) {
                res.sendStatus(404);
                return;
            }
    
            res.json(removedEntity); 
        } catch (error) {
            res.status(500).json({ error });
        }
    }

    protected _parseRequsetQuery(query: {[key: string]: any}): ParsedQuery {
        const limit: number = query.limit ? parseInt(query.limit) : 0;
        const offset: number = query.offset ? parseInt(query.offset) : 0;

        const filter: {[key: string]: any} = query.filter 
            ? JSON.parse(query.filter) 
            : {};

        const sort: {[key: string]: number} = (query.sort && query.order) 
            ? {[query.sort]: parseInt(query.order)}
            : {};

        return { limit, offset, filter, sort };
    }
}
