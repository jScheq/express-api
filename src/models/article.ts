import { Schema, model } from "mongoose";
import { Article } from "../interfaces/article";


const articleSchema: Schema = new Schema(
    {
        name: { type: String, required: true },
        slug: { type: String, required: true, unique: true },
        text: { type: String, minlength: 5, maxlength: 5000 },
        author: { type: Schema.Types.ObjectId, ref: 'User' },
        category: [{ value: String }],
        comments: [
            {  
                comment: { type: String, required: true }, 
                date: { type: Date, default: Date.now }, 
                author: { type: Schema.Types.ObjectId, ref: 'User' } 
            }
        ],
    }, 
    { timestamps: true }
);

export default model<Article>('Article', articleSchema);
