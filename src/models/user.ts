import { Schema, model } from "mongoose";
import { User } from "../interfaces/user";


const userSchema: Schema = new Schema({
    name: String,
    email: String,
    password: String
});

export default model<User>('User', userSchema);
