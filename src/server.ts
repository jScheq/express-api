import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as morgan from 'morgan';
import * as express from 'express';
import * as mongoose from 'mongoose';
import { Application } from 'express';
import bodyParser = require('body-parser');
import { DotenvConfigOutput } from 'dotenv';
import UserRouterHandler from './routers/user';
import ArticleRouterHandler from './routers/artcile';


export class Server {
    private _app: Application;

    constructor() {
        this._app = express();
    }

    start(): void {
        this._loadEnv();
        this._setConnection();
        this._setMiddleware();
        this._setRouters();

        this._app.listen(
            process.env.PORT, 
            () => console.log(`Сервер стартовал на порту ${process.env.PORT}!`)
        );
    }

    private _setConnection(): void {
        mongoose
            .connect(process.env.DB_CONNECTION,  { useNewUrlParser: true })
            .then(() => console.log('Успешное подключение к БД'))
            .catch(err => console.log('Произошла ошибка\n', err));
    }

    private _setRouters(): void {
        this._app.use('/api/users', new UserRouterHandler().router);
        this._app.use('/api/articles', new ArticleRouterHandler().router);
    }

    private _setMiddleware(): void {
        this._app.use(
            cors({ 
                origin: process.env.ORIGIN, 
                exposedHeaders: ['Content-Range'] 
            })
        );
        
        this._app.use(bodyParser.json());
        
        this._app.use(morgan('tiny'));
    }

    private _loadEnv(): void {
        const config: DotenvConfigOutput = dotenv.config();
        if (config.error) {
            console.log('Локальный .env не найден')
        }
    }
}